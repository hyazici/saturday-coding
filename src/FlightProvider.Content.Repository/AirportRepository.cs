﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using Dapper;
using FlightProvider.Content.Repository.Interface;
using FlightProvider.Content.Repository.Model;

namespace FlightProvider.Content.Repository
{
    public class SqLiteBaseRepository
    {
        public static string DbFile
        {
            get { return Environment.CurrentDirectory + "\\SimpleDb.sqlite"; }
        }

        public static SQLiteConnection SimpleDbConnection()
        {
            return new SQLiteConnection("Data Source=" + DbFile);
        }
    }
    public class AirportRepository : SqLiteBaseRepository, IAirportRepository
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["FlightProviderDb"].ToString();
        public AirportRepository()
        {
            CreateAirportTableIfNotExists();
        }

        public IEnumerable<Airport> GetAirports()
        {
            using (var connection = SimpleDbConnection())
            {
                return connection.Query<Airport>("SELECT * FROM Airport  ");
            }
        }

        public IEnumerable<Airport> GetAirpotyLikeName(string name)
        {
            using (var connection = SimpleDbConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Name", string.Concat("%",name,"%"));
                return connection.Query<Airport>("SELECT * FROM Airport  WHERE Name Like @Name ", parameters);
            }
        }

        public Airport GetByCode(string code)
        {
            using (var connection = SimpleDbConnection())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Code", code);
                return connection.Query<Airport>("SELECT * FROM Airport  WHERE Code=@Code ", parameters).FirstOrDefault();
            }
        }

        public void Insert(Airport airport)
        {
            using (var connection = SimpleDbConnection())
            {
                connection.Execute(
                    "INSERT INTO Airport (Code,Name,Country) VALUES (@Code,@Name,@Country)",
                    airport
                    );

            }
        }

        public void Update(Airport airport)
        {
            using (var connection = SimpleDbConnection())
            {
                connection.Execute(
                    "UPDATE Airport SET  Name=@Name,Country=@Country WHERE Code=@Code",
                    new
                    {
                        airport.Name,
                        airport.Country,
                        airport.Code
                    }
                    );
            }
        }
        public void Delete(string airportCode)
        {
            using (var connection = SimpleDbConnection())
            {
                connection.Execute(
                    "DELETE FROM Airport WHERE Code=@Code",
                    airportCode
                    );
            }
        }

        public void CreateAirportTableIfNotExists()
        {
            //File.Delete(DbFile);
            if (!File.Exists(DbFile))
            {

                using (var connection = SimpleDbConnection())
                {
                    connection.Execute(
                        "CREATE TABLE Airport(Code VARCHAR(3) PRIMARY KEY ASC,Name VARCHAR(50),Country VARCHAR(50));"
                        );
                }
            }
        }
    }
}