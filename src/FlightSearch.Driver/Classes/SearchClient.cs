﻿using System;
using System.Collections.Generic;

namespace FlightSearch.Driver.Classes
{
   public class SearchClient
    {
       private readonly IFlight _flightClient;
       public SearchClient(IFlight flightClient)
       {
           _flightClient = flightClient;
       }

       public List<FlightSearchResult> Search(FlightSearchCriteria criteria)
       {
           List<FlightSearchResult> result = _flightClient.Search(new FlightSearchCriteria
           {
               DepartureDate = new DateTime(2013, 12, 31),
               ReturnDate = new DateTime(2014, 01, 02),
               FromCity = "IST",
               ToCity = "AYT",
               Adult = 2,
               Child = 0
           });
            return result;
       }
    }
}
