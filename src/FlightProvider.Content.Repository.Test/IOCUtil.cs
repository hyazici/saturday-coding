﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using FlightProvider.Content.Repository.Interface;

namespace FlightProvider.Content.Repository.Test
{
    public static class IoCUtil
    {
        private static IWindsorContainer _container = null;
        private static IWindsorContainer Container
        {
            get
            {
                if (_container == null)
                {
                    _container = BootstrapContainer();
                }
                return _container;
            }
        }
        private static IWindsorContainer BootstrapContainer()
        {
                        
            return new WindsorContainer().Register(
                    Component.For<IAirportRepository>().ImplementedBy<AirportRepository>().LifestyleTransient()
             //        Component.For<IAirportRepository>().UsingFactoryMethod(x => new AirportRepositoryWithCache(new AirportRepository())).LifestyleSingleton()
                    );
        }
        public static T Resolve<T>()
        {
            return Container.Resolve<T>();

        }
    }
}
