﻿namespace ThermalHotel
{
    public class Breakfast : IThermalHotel
    {
        private readonly IThermalHotel _thermalHotel;

        public Breakfast(IThermalHotel thermalHotel)
        {
            _thermalHotel = thermalHotel;
        }

        public double GetPrice()
        {
            return _thermalHotel.GetPrice() + 5;
        }

        public string GetDescription()
        {
            return "Breakfast " + _thermalHotel.GetDescription();
        }
    }
}