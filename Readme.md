* 21.12.2013 - Builder Pattern and Fluent Interface - by Vitaliy Tsvayer
* 28.12.2013 - Dependency Injection Pattern - by Koray Uysal
* 04.01.2014 - Decorator Pattern - by Haluk Yazici
* 11.01.2014 - Introduction to DVCSs like Git/Mercurial and best practices - by Serol Güzel
* 18.01.2014 - Practical TDD with the help of Resharper/NCrunch/etc - by Vitaliy Tsvayer
* 25.01.2014 - Intriduction to SignalR - by Batuhan Kalkan
* ??? - Introduction to Amazon AWS
